<?php
/**
 * The template for displaying Category Archive pages.
 *
 * @package WordPress
 * @subpackage Perfect
 * @since Perfect
 */

get_header(); ?>
<?php $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); ?>
<div class="title">
	<h2><?php echo $term->name ?></h2>
</div>
<div class="content">
<?php if (have_posts()): ?>
	<?php while (have_posts()) :	the_post(); ?>
	<div class="services">
		<a href="<?php the_permalink(); ?>" class="service_thumb"><?php the_post_thumbnail('posts-thumb'); ?></a>
		<div class="service_det">
			<h5><?php the_title(); ?></h5>
			<?php the_excerpt(); ?>
			<a href="<?php the_permalink(); ?>" class="<?php _e('read_more', 'perfect') ?>"><?php _e('MORE', 'perfect') ?></a>
		</div>
	</div>
	<?php endwhile; ?>
<?php endif; ?>
	<?php echo wp_pagenavi(); ?>
</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
