<?php
/**
 * The main template file.
 *
 * @package WordPress
 * @subpackage Perfect
 * @since Perfect 1.0
 */

get_header(); ?>
			<div class="page_slogan">
				<h1><?php echo get_cat_name( $_GET['cat'] ); ?></h1>
			</div>
<div class="content">
<?php if (have_posts()): ?>
	<?php while (have_posts()) : the_post(); ?>
	<div class="services">
		<a href="<?php the_permalink(); ?>" class="service_thumb"><?php the_post_thumbnail('posts-thumb'); ?></a>
		<div class="service_det">
			<h5><?php the_title(); ?></h5>
			<?php the_excerpt(); ?>
			<a href="<?php the_permalink(); ?>" class="<?php _e('read_more', 'perfect') ?>"><?php _e('Leer más', 'perfect') ?></a>
		</div>
	</div>
	<?php endwhile; ?>
<?php endif; ?>
<?php echo wp_pagenavi(); ?>
</div>
<?php get_footer(); ?>