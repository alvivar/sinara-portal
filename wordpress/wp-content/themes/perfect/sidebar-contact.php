<?php
/**
 * @package WordPress
 * @subpackage Perfect
 */
?>
<div class="sidebar">
	<div class="widget">
		<div class="title"><h3><?php _e('Dirección de contacto') ?></h3></div>
		<?php $address = get_post_meta($post->ID, 'Address', $single = true); ?>
		<?php echo $address; ?>
	</div>
</div>
