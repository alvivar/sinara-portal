<?php
/**
 * The main template file.
 *
 * @package WordPress
 * @subpackage Perfect
 * @since Perfect 1.0
 */

get_header(); ?>
<div class="page_slogan">
	<h1>Providing leading business services</h1>
</div>
<div class="content">


	<?php
/**
 * The loop that displays posts.
 */
if (have_posts()): ?>
	
	<?php if ( is_search() ) : ?>
		<h1 class="page-title pd-left"><?php printf( __( 'Search Results for: %s', 'theme1126' ), '<span>' . get_search_query() . '</span>' ); ?></h1><br>
	<?php endif; ?>
	
	<?php while (have_posts()) : the_post();?>
		<?php if (is_single()) { include (TEMPLATEPATH . '/navigation.php'); } ?>

			
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
				<strong><?php echo get_the_date('F j, Y'); ?></strong><br>
			
				<span class="cat-links">
					<?php printf( __( '<span class="%1$s">Posted in</span> %2$s', 'theme1163' ), '', get_the_category_list( ', ' ) ); ?>
				</span>
			
				<?php if ( (is_archive()) or (is_search()) or (is_paged()) or (is_category()) ) : ?>
					<div class="postmetadata"><?php the_tags('<span>Tags:</span>', ', '); ?></div>
				<?php endif; ?>
			
				<div class="entry-content">
					<?php the_excerpt(); ?>
				</div><!-- .entry-content -->
			
				<div class="entry-utility hentry">
					<a class="read-more" href="<?php the_permalink(); ?>"><span><?php _e('Leer más') ?></span></a>
				</div>
			<br>
			</div><!-- #post-## --><br>
						
	<?php endwhile; ?>

	<div id="nav-below" class="navigation wrapper">
		<?php if(function_exists('wp_paginate')) {
			wp_paginate();
		} ?>
	</div><!-- #nav-below -->
	
	<?php else : ?>
		
			
					<h1 class="page-title"><?php _e( 'Nothing Found'); ?></h1>
						<div class="entry-content">
							<p><?php _e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'theme1126' ); ?></p>
							<div class="wrapper">
								<?php get_search_form(); ?>
							</div>
						</div><!-- .entry-content -->
			
		
<?php endif; ?>

	
	
	<?php if (have_posts()): ?>
		<?php while (have_posts()) :	the_post(); ?>
		<div class="services">
			<a href="<?php the_permalink(); ?>" class="service_thumb"><?php the_post_thumbnail('posts-thumb'); ?></a>
			<div class="service_det">
				<h5><?php the_title(); ?></h5>
				<?php the_excerpt(); ?>
				<a href="<?php the_permalink(); ?>" class="<?php _e('read_more', 'perfect') ?>"><?php _e('MORE', 'perfect') ?></a>
			</div>
		</div>
		<?php endwhile; ?>
	<?php endif; ?>
	
	<?php echo wp_pagenavi(); ?>
	
</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
