<?php class RecentPosts1Widget extends WP_Widget
{
	 function RecentPosts1Widget(){
		$widget_ops = array();
		$control_ops = array('width' => 400, 'height' => 300);
		parent::WP_Widget(false, $name='Recent Posts (with thubnail) 1', $widget_ops, $control_ops);
	 }

  /* Displays the Widget in the front-end */
	 function widget($args, $instance){
		extract($args);
		$title = apply_filters('widget_title', empty($instance['title']) ? 'Posts' : $instance['title']);

		echo $before_widget;

		if ( $title )
		echo 
			$before_title . 
			"<a href='?cat=35'>" . $title . "</a>" .
			$after_title;


		$selected = new WP_Query(array('post_type' => 'post', 'category_name' => 'convocatorias', 'orderby' => 'post_date', 'order' => 'DESC', 'posts_per_page' => 4));
		if($selected->have_posts()) : ?>
			<ul class="news">
			<?php while($selected->have_posts()) :	$selected->the_post(); ?>
				<li>
					<div class="news_excerpt">
						<a class="news_thumb" href="<?php the_permalink() ?>"><?php the_post_thumbnail('recent-thumb'); ?></a>
						<span class="news_date"><?php echo get_the_date('D, F j'); ?></span>
						<?php the_excerpt(); ?>
					</div>
				</li>
			<?php endwhile; ?>
			</ul>
		<?php endif;

		echo $after_widget;
	}

  /*Saves the settings. */
	function update($new_instance, $old_instance){
		$instance = $old_instance;
		$instance['title'] = stripslashes($new_instance['title']);

		return $instance;
	}

  /*Creates the form for the widget in the back-end. */
	 function form($instance){
		//Defaults
		$instance = wp_parse_args( (array) $instance, array('title'=>'Recent Posts') );

		$title = htmlspecialchars($instance['title']);

		# Title
		echo '<p><label for="' . $this->get_field_id('title') . '">' . 'Title:' . '</label><input class="widefat" id="' . $this->get_field_id('title') . '" name="' . $this->get_field_name('title') . '" type="text" value="' . $title . '" /></p>';
	}

}// end RecentPostsWidget class

function RecentPostsWidgetInit1() {
  register_widget('RecentPosts1Widget');
}

add_action('widgets_init', 'RecentPostsWidgetInit1');

?>