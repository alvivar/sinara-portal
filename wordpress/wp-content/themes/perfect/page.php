<?php
/**
 * @package WordPress
 * @subpackage Perfect
 */
/*
 Template Name: Default Page Layout
 */
?>
<?php get_header(); ?>
<div class="gko_menu_inside">
	<?php get_sidebar('menu'); ?>
</div>
<div class="gko_content_inside">
	<?php if ( have_posts() ): the_post(); ?>
		<div class="title">
			<h2><?php the_title(); ?></h2>
		</div>
		<?php the_content(); ?>
	<?php endif; ?>
</div>
<?php get_footer(); ?>