<?php
/**
 * @package WordPress
 * @subpackage Perfect
 */
	if ( !is_active_sidebar( 'support-widget-area' ))
		return;
?>
<div class="sidebar">
	<?php if ( is_active_sidebar( 'support-widget-area' ) ) : ?>
		<?php dynamic_sidebar( 'support-widget-area' ); ?>
	<?php endif; ?>
</div>
