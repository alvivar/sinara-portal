<?php
/*
Template Name: Support Page Layout
*/
get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<div class="page_slogan">
		<?php $custom = get_post_custom($post->ID); ?>
		<?php $slogan = (empty($custom['slogan'][0]) ? '' : $custom['slogan'][0]); ?>
		<h1><?php echo $slogan; ?></h1>
	</div>
	<div class="content">
		<div class="title">
			<h2><?php the_title(); ?></h2>
		</div>
		<?php the_content(); ?>
	</div>
<?php endwhile; ?>
<?php get_sidebar('support'); ?>
<?php get_footer(); ?>