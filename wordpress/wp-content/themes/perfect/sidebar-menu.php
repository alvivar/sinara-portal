<?php
/**
 * @package WordPress
 * @subpackage Perfect
 */
if (!is_active_sidebar('menu-widget-area'))
	return;
?>
<div class="menubar">
	<?php if ( is_active_sidebar( 'menu-widget-area' ) ) :
	?>
	<?php dynamic_sidebar('menu-widget-area');?>
	<?php endif;?>
</div>
