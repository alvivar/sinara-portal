<?php
/**
 * @package WordPress
 * @subpackage Perfect
 */
	if ( !is_active_sidebar( 'main-widget-area' ))
		return;
?>
<div class="sidebar">
	<?php dynamic_sidebar( 'main-widget-area' ); ?>
</div>