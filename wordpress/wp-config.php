<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'sirea');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '~PwNy($0MpSelZl?Yb+6q@n251A536_PBT#(~`s!An9.JEW[`WtQW9RG[8ejm#XO');
define('SECURE_AUTH_KEY',  'LUN`*FwUR^Z:,EzWgG;+H2|jt1~6~{rESwh5Zc.`4 `)XSb0LVSJa<O:E|/*@cn^');
define('LOGGED_IN_KEY',    '-I2%E9%n@shZF*p]M:wVr7jgV+pnH^npn^n)y}lM_%E5vFPU/JJ+U5|Oi3oAu|#W');
define('NONCE_KEY',        '6RMcUjXAR;J.s2;4#~RgvuD(9B/{9HJ6ZH?$JlA;eR]WeE(^2~>)zKGDkt0PJ*yD');
define('AUTH_SALT',        '[F/qorXc4KWsK0{@9=.)|me;u(x^W0I-lBI}=*_]l:8X=+vd~lo5,{C`/3+gF )7');
define('SECURE_AUTH_SALT', '!=aYi&/p~;}Jvw4A-wq8cF)$1xBa!Jsr(X5`z*EV=_;8Op[Ij6~oo[aYx4,gBi7@');
define('LOGGED_IN_SALT',   'r/@ootX.:%CF*f/U=T];|zt&7-+0}:F31cg-vf0F z?a{G3E%d,Yucq`&>1r0r$|');
define('NONCE_SALT',       'xfdfUO1R^C*9lre9+<46d`Ppf:[*e^;.5FH8`((@W#Vo|}y<J*BrFebhZXbc2n!9');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
