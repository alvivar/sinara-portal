<?php
/**
 * The Header for our theme.
 *
 * @package WordPress
 * @subpackage Perfect
 * @since Perfect 1.0
 */
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head>
<meta http-equiv="content-type" content="text/html;charset=<?php bloginfo( 'charset' ); ?>" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'perfect' ), max( $paged, $page ) );

	?></title>

	<!--[if !IE 7]>
		<style type="text/css">
			.container {display:table; height:100%;}
		</style>
	<![endif]-->

	<?php //wp_enqueue_style('main'); ?>
	<?php wp_enqueue_script('jquery'); ?>
	<!-- link to the JavaScript files (hoverIntent is optional)  -->
	<?php wp_enqueue_script('hover_intent'); ?>
	<?php wp_enqueue_script('superfish'); ?>

	<?php if ( is_home() || is_front_page() ) wp_enqueue_script('slideshow'); ?>
	<?php if (is_page(107)) wp_enqueue_script('watermarkinput'); ?>
	<?php if (is_page(164)) wp_enqueue_script('jcarousel'); ?>
	<?php if (is_page(109)) wp_enqueue_script('fade'); ?>

<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>

<?php if (is_page(107)): ?>
	<script type="text/javascript">
		jQuery(function($){
			$("#name").Watermark("Name");
			$("#subj").Watermark("Subject");
			$("#email").Watermark("Email");
		});
	</script>
<?php endif; ?>

<?php if (is_page(164)): ?>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			jQuery('#mycarousel').jcarousel();
		});
	</script>
<?php endif; ?>



	<!-- initialise Superfish -->
	<script type="text/javascript">

		jQuery(document).ready(function($){
			$("ul.sf-menu").superfish();
		});

	</script>
	
    <style type="text/css">@import url("<?php bloginfo('stylesheet_directory'); ?>/style.css");</style>
    <link rel="alternate stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/stylesheets/style-blue.css" title="style-blue" media="screen" />
    <link rel="alternate stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/stylesheets/style-green.css" title="style-green" media="screen" />
    <link rel="alternate stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/stylesheets/style-orange.css" title="style-orange" media="screen" />

	<script src="<?php bloginfo('stylesheet_directory'); ?>/js/styleswitch.js" type="text/javascript"></script>

</head>

<body>
<div class="container">
	<div class="top">
		<ul class="top_menu">
			<!-- gkHACK <li><a href="#" class="icon1" title="Flickr"></a></li>-->
			<!--<li><a href="#" class="icon2" title="Twitter"></a></li>
			<li><a href="#" class="icon3" title="RSS Feed"></a></li>-->
		</ul>
		<?php wp_nav_menu(array(
				  'container' => false,
				  'theme_location' => 'top',
		)); ?>
		
		<?php echo date('d/m/y'); ?>
		<div class="clear"></div>
	</div>
	<div id="header">
		<div class="head">
			<div class="logo">
				<!-- gkHACK
					 HEADER PNG, Flash, etc
				<a href="<?php echo home_url( '/' ); ?>" class="logo"><?php bloginfo( 'name' ); ?></a>
				<span class="slogan"><?php _e('La ruta hacia la competitividad', 'perfect') ?></span>
				-->
			</div>
			<!-- gkHACK
			<div class="welcome_msg"><?php _e('Default Welcome MSG!', 'perfect') ?></div>
			<ul class="log_reg">
				<li><a href="<?php echo site_url('wp-login.php', 'login') ?>"><?php _e('Login', 'perfect') ?></a></li>
				<?php if (get_option('users_can_register')) : ?>
				<li><a href="<?php echo site_url('wp-login.php?action=register', 'login') ?>"><?php _e('Register', 'perfect') ?></a></li>
				<?php endif; ?>
			</ul>
			-->
			<div class="clear"></div>
		</div>
<?php
if ( is_home() || is_front_page() ): ?>
	<?php $loop = new WP_Query(array('post_type' => 'primary', 'posts_per_page' => 15)); ?>
	<?php if ($loop->have_posts()): ?>
		<div id="tmpSlideshow">
		<?php $posts_counter = 0; ?>
		<?php while ($loop->have_posts()) :	$loop->the_post(); $posts_counter++; ?>
			<div id="tmpSlide-<?php echo $posts_counter; ?>" class="tmpSlide">
				<?php the_post_thumbnail('slider-thumb'); ?>
				<div class="tmpSlideCopy">
					<h2><?php the_title(); ?></h2>
					<a href="<?php the_permalink(); ?>" class="more_btn" title="<?php _e('Leer más', 'perfect') ?>"><?php _e('leer más', 'perfect') ?></a>
				</div>
			</div>
		<?php endwhile; ?>
		<?php if ($posts_counter > 0): ?>
			<div id="tmpSlideshowControls">
			<?php for ($i = 1; $i <= $posts_counter; $i++): ?>
			  <div class="tmpSlideshowControl" id="tmpSlideshowControl-<?php echo $i; ?>"><?php echo $i; ?></div>
			 <?php endfor; ?>
			</div>
		<?php endif; ?>
		</div>
	<?php endif; ?>
<?php endif; ?>
	</div>
	<div id="menu">
	<?php wp_nav_menu(array(
			  'container' => false,
			  'menu_class' => 'sf-menu',
			  'theme_location' => 'primary',
	));
	function start_el(&$output, $item, $depth, $args) {
		global $wp_query;
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$class_names = $value = '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;

		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
		$class_names = ' class="' . esc_attr( $class_names ) . '"';

		$output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';

		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
		$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

		$item_output = $args->before;
		$item_output .= '<a'. $attributes .'>';
		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
		$item_output .= '</a>';
		$item_output .= $args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}?>
	<div class="clear"></div>
	</div>
	<div id="content">
		<div class="cont_container">
