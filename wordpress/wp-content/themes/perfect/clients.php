<?php
/*
Template Name: Clients Page Layout
*/
get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<div class="page_slogan">
		<?php $custom = get_post_custom($post->ID); ?>
		<?php $slogan = (empty($custom['slogan'][0]) ? '' : $custom['slogan'][0]); ?>
		<h1><?php echo $slogan; ?></h1>
	</div>
	<div class="title">
		<h2><?php the_title(); ?></h2>
	</div>
	<?php echo the_content(); ?>
<?php $loop = new WP_Query(array('post_type' => 'clients', 'posts_per_page' => 9999)); ?>
<?php if ($loop->have_posts()): ?>
	<ul id="clients" class="clients">
	<?php while ($loop->have_posts()) :	$loop->the_post(); ?>
		<?php $custom = get_post_custom($post->ID); ?>
		<?php $website_url = (empty($custom['website_url'][0]) ? '' : $custom['website_url'][0]); ?>
		<li><a href="<?php echo $website_url; ?>"><?php the_post_thumbnail('clients-thumb'); ?></a></li>
	<?php endwhile; ?>
<?php endif; ?>
<?php endwhile; ?>
<?php get_footer(); ?>