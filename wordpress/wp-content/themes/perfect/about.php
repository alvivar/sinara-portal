<?php
/*
Template Name: About Page Layout
*/
get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<div class="page_slogan">
		<?php $custom = get_post_custom($post->ID); ?>
		<?php $slogan = (empty($custom['slogan'][0]) ? '' : $custom['slogan'][0]); ?>
		<h1><?php echo $slogan; ?></h1>
	</div>
<?php $loop = new WP_Query(array('post_type' => 'staff', 'posts_per_page' => 9999)); ?>
<?php if ($loop->have_posts()): ?>
	<ul class="personnel">
	<?php while ($loop->have_posts()) :	$loop->the_post(); ?>
		<li>
			<a href="<?php the_permalink(); ?>">
				<span class="person_pic"><?php the_post_thumbnail('staff-thumb'); ?></span>
				<?php $custom = get_post_custom($post->ID); ?>
				<?php $name = (empty($custom['staff_name'][0]) ? '' : $custom['staff_name'][0]); ?>
				<span class="person_title"><?php echo $name; ?></span>
				<?php
					$depts = array();
					$terms = get_the_terms($post->ID, 'department');
					if (!empty($terms)) {
						foreach($terms as $term) {
							$depts[] = $term->name;
						}
						$depts = implode(',', $depts) . ' dpt';
						echo '<span class="person_dpt">' . $depts . '</span>';
					}
				?>
				<span class="person_short"><?php the_excerpt(); ?></span>
			</a>
		</li>
	<?php endwhile; ?>
	</ul>
<?php endif; ?>
<?php endwhile; ?>
<?php get_footer(); ?>