<?php
/**
 * Perfect functions and definitions
 *
 *
 * @package WordPress
 * @subpackage Perfect
 * @since Perfect 1.0
 */

 /** Tell WordPress to run perfect_setup() when the 'after_setup_theme' hook is run. */
add_action( 'after_setup_theme', 'perfect_setup' );

if ( ! function_exists( 'perfect_setup' ) ) {
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 *
 * To override perfect_setup() in a child theme, add your own perfect_setup to your child theme's
 * functions.php file.
 *
 * @uses add_theme_support() To add support for post thumbnails and automatic feed links.
 * @uses register_nav_menus() To add support for navigation menus.
 * @uses add_custom_background() To add support for a custom background.
 * @uses add_editor_style() To style the visual editor.
 * @uses load_theme_textdomain() For translation/localization support.
 * @uses add_custom_image_header() To add support for a custom header.
 * @uses register_default_headers() To register the default custom header images provided with the theme.
 * @uses set_post_thumbnail_size() To set a custom post thumbnail size.
 *
 * @since Perfect 1.0
 */
	function perfect_setup() {

		// This theme styles the visual editor with editor-style.css to match the theme style.
		add_editor_style();

		// This theme uses post thumbnails
		add_theme_support( 'post-thumbnails' );

		// Make theme available for translation
		// Translations can be filed in the /languages/ directory
		load_theme_textdomain( 'perfect', TEMPLATEPATH . '/languages' );

		$locale = get_locale();
		$locale_file = TEMPLATEPATH . "/languages/$locale.php";
		if ( is_readable( $locale_file ) )
			require_once( $locale_file );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'top' => __( 'Top Navigation', 'perfect' ),
			'primary' => __( 'Primary Navigation', 'perfect' ),
			'footer' => __( 'Footer Navigation', 'perfect' ),
		) );

	}
}


/** Register sidebars by running perfect_widgets_init() on the widgets_init hook. */
add_action( 'widgets_init', 'perfect_widgets_init' );
/**
 * Register widgetized areas, including two sidebars and four widget-ready columns in the footer.
 *
 * To override perfect_widgets_init() in a child theme, remove the action hook and add your own
 * function tied to the init hook.
 *
 * @since Perfect 1.0
 * @uses register_sidebar
 */
function perfect_widgets_init() {
	register_sidebar( array(
		'name' => __( 'Main Widget Area', 'perfect' ),
		'id' => 'main-widget-area',
		'description' => __( 'The main widget area', 'perfect' ),
		'before_widget' => '<div class="widget">',
		'after_widget' => '</div>',
		'before_title' => '<div class="title"><h3>',
		'after_title' => '</h3></div>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Menu Widget Area', 'perfect' ),
		'id' => 'menu-widget-area',
		'description' => __( 'The menu widget area', 'perfect' ),
		'before_widget' => '<div class="widget">',
		'after_widget' => '</div>',
		'before_title' => '<div class="title"><h3>',
		'after_title' => '</h3></div>',
	) );

	register_sidebar( array(
		'name' => __( 'Home Widget Area', 'perfect' ),
		'id' => 'home-widget-area',
		'description' => __( 'The home widget area', 'perfect' ),
		'before_widget' => '<div class="widget">',
		'after_widget' => '</div>',
		'before_title' => '<div class="title"><h3>',
		'after_title' => '</h3></div>',
	) );

	register_sidebar( array(
		'name' => __( 'Support Widget Area', 'perfect' ),
		'id' => 'support-widget-area',
		'description' => __( 'The support widget area', 'perfect' ),
		'before_widget' => '<div class="widget">',
		'after_widget' => '</div>',
		'before_title' => '<div class="title"><h3>',
		'after_title' => '</h3></div>',
	) );

	register_sidebar( array(
		'name' => __( 'Footer Widget Area', 'perfect' ),
		'id' => 'footer-widget-area',
		'description' => __( 'The footer widget area', 'perfect' ),
		'before_widget' => '<div class="bot_widget">',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	) );

}
// widgets
include(TEMPLATEPATH . '/widgets/widget-recent-1.php');
include(TEMPLATEPATH . '/widgets/widget-recent-2.php');

// thumb sizes
if ( function_exists( 'add_image_size' ) ) {
		  add_image_size( 'recent-thumb', 90, 90, true);
		  add_image_size( 'posts-thumb', 214, 158, true );
		  add_image_size( 'clients-thumb', 233, 159, true );
		  add_image_size( 'single-thumb', 234, 178, true );
		  add_image_size( 'staff-thumb', 287, 218, true );
		  add_image_size( 'projects-thumb', 287, 287, true );
		  add_image_size( 'slider-thumb', 626, 385, true );
}


// custom Post Types
add_action('init', 'custom_post_types');
function custom_post_types() {
	// Slider
	register_post_type(
		'primary',
		array(
			'label' => __('Slider', 'perfect'),
			'public' => true,
			'show_ui' => true,
			'hierarchical' => false,
			'rewrite' => true,
			'exclude_from_search' => false,
			'supports' => array(
				'title',
				'editor',
				'page-attributes',
				'thumbnail',
			),
		)
	);
	// Info panel
	$labels = array(
		'name' => _x('Info Panel', 'post type general name'),
		'singular_name' => _x('Info', 'post type singular name'),
		'add_new' => _x('Add New', 'faqs'),
		'add_new_item' => __('Add New Info'),
		'edit_item' => __('Edit Info'),
		'new_item' => __('New Info'),
		'view_item' => __('View Info'),
		'search_items' => __('Search Info'),
		'not_found' =>  __('No Info found'),
		'not_found_in_trash' => __('No Info found in Trash'),
		'parent_item_colon' => ''
	);
	register_post_type(
		'foundation',
		array(
			'labels' => $labels,
			'public' => true,
			'show_ui' => true,
			'hierarchical' => false,
			'rewrite' => true,
			'exclude_from_search' => true,
			'show_in_nav_menus' => false,
			'supports' => array(
				'title',
				'excerpt',
				'editor',
			),
		)
	);
	// Projects
	/*
	$labels = array(
		'name' => _x('Projects', 'post type general name'),
		'singular_name' => _x('Project', 'post type singular name'),
		'add_new' => _x('Add New', 'project'),
		'add_new_item' => __('Add New Project'),
		'edit_item' => __('Edit Project'),
		'new_item' => __('New Project'),
		'view_item' => __('View Project'),
		'search_items' => __('Search Projects'),
		'not_found' =>  __('No projects found'),
		'not_found_in_trash' => __('No projects found in Trash'),
		'parent_item_colon' => ''
	);
	register_post_type(
		'projects',
		array(
			'labels' => $labels,
			'public' => true,
			'show_ui' => true,
			'hierarchical' => false,
			'rewrite' => true,
			'exclude_from_search' => false,
			'supports' => array(
				'title',
				'excerpt',
				'editor',
				'thumbnail',
				'page-attributes',
				'comments'
			),
			'taxonomies' => array('project_category'),
		)
	);
	*/
	// Personals
	$labels = array(
		'name' => _x('Staff', 'post type general name'),
		'singular_name' => _x('Staff', 'post type singular name'),
		'add_new' => _x('Add New', 'staff'),
		'add_new_item' => __('Add New Staff'),
		'edit_item' => __('Edit Staff'),
		'new_item' => __('New Staff'),
		'view_item' => __('View Staff'),
		'search_items' => __('Search Staff'),
		'not_found' =>  __('No staff found'),
		'not_found_in_trash' => __('No staff found in Trash'),
		'parent_item_colon' => ''
	);
	register_post_type(
		'staff',
		array(
			'labels' => $labels,
			'public' => true,
			'show_ui' => true,
			'hierarchical' => false,
			'rewrite' => true,
			'exclude_from_search' => false,
			'supports' => array(
				'title',
				'excerpt',
				'editor',
				'thumbnail',
				'page-attributes',
				'comments'
			),
			'taxonomies' => array('department'),
		)
	);
	// custom taxonomies
	register_taxonomy(
		'project_category',
		'projects',
		array(
			'hierarchical' => true,
			'label' => __('Project Categories'),
			'singular_label' => __('Project Category'),
			'query_var' => true,
			'rewrite' => true,
		)
	);
	register_taxonomy(
		'department',
		array(
			'staff',
			'projects',
		),
		array(
			'hierarchical' => true,
			'label' => __('Departments'),
			'singular_label' => __('Department'),
			'query_var' => true,
			'rewrite' => true,
		)
	);
	// FAQ
	$labels = array(
		'name' => _x('FAQ', 'post type general name'),
		'singular_name' => _x('FAQ', 'post type singular name'),
		'add_new' => _x('Add New', 'faqs'),
		'add_new_item' => __('Add New FAQ'),
		'edit_item' => __('Edit FAQ'),
		'new_item' => __('New FAQ'),
		'view_item' => __('View FAQ'),
		'search_items' => __('Search FAQs'),
		'not_found' =>  __('No FAQs found'),
		'not_found_in_trash' => __('No FAQs found in Trash'),
		'parent_item_colon' => ''
	);
	register_post_type(
		'faqs',
		array(
			'labels' => $labels,
			'public' => true,
			'show_ui' => true,
			'hierarchical' => false,
			'rewrite' => true,
			'exclude_from_search' => false,
			'supports' => array(
				'title',
				'page-attributes',
			),
		)
	);
	// Links
	$labels = array(
		'name' => _x('Links', 'post type general name'),
		'singular_name' => _x('Link', 'post type singular name'),
		'add_new' => _x('Add New', 'Link'),
		'add_new_item' => __('Add New Link'),
		'edit_item' => __('Edit Link'),
		'new_item' => __('New Link'),
		'view_item' => __('View Link'),
		'search_items' => __('Search Link'),
		'not_found' =>  __('No Link found'),
		'not_found_in_trash' => __('No Link found in Trash'),
		'parent_item_colon' => ''
	);
	register_post_type(
		'clients',
		array(
			'labels' => $labels,
			'public' => true,
			'show_ui' => true,
			'hierarchical' => false,
			'rewrite' => true,
			'exclude_from_search' => true,
			'supports' => array(
				'title',
				'thumbnail',
			)
		)
	);

}
// end custom Post Types

// custom Post Fields
add_action('admin_init', 'add_meta_page_box');
function add_meta_page_box(){
// Slogan for Pages
	add_meta_box('slogan', __('Slogan', 'perfect'), 'page_options', 'page', 'normal', 'high');
// Answer for FAQ
	add_meta_box('faq_answer', __('Answer', 'perfect'), 'faqs_ans_options', 'faqs', 'normal', 'high');
// Name for Projects
	add_meta_box('proj_name', __('Name', 'perfect'), 'proj_name_options', 'projects', 'normal', 'high');
// Name for Staff
	add_meta_box('staff_name', __('Name', 'perfect'), 'staff_name_options', 'staff', 'normal', 'high');
// URL for Clients
	add_meta_box('website_url', __('URL', 'perfect'), 'website_url_options', 'clients', 'normal', 'high');
}
// Slogan options
function page_options(){
	global $post;
	$custom = get_post_custom($post->ID);
	$slogan = (empty($custom['slogan'][0]) ? '' : $custom['slogan'][0]);
	echo '<label class="screen-reader-text" for="slogan">' . __('Slogan', 'perfect') . '</label><textarea rows="1" cols="40" name="slogan" tabindex="6" id="slogan" style="width: 99%;">' . $slogan . '</textarea>';
}
// Answer options
function faqs_ans_options(){
	global $post;
	$custom = get_post_custom($post->ID);
	$faq_answer = (empty($custom['faq_answer'][0]) ? '' : $custom['faq_answer'][0]);
	echo '<label class="screen-reader-text" for="faq_answer">' . __('Answer', 'perfect') . '</label><textarea rows="1" cols="40" name="faq_answer" tabindex="6" id="faq_answer" style="width: 99%;">' . $faq_answer . '</textarea>';
}

add_filter('manage_edit-faqs_columns', 'faqs_edit_columns');
function faqs_edit_columns($faqs_columns){
	$faqs_columns['title'] = __('Question', 'perfect');
	return $faqs_columns;
}
// Projects Name options
function proj_name_options(){
	global $post;
	$custom = get_post_custom($post->ID);
	$proj_name = (empty($custom['proj_name'][0]) ? '' : $custom['proj_name'][0]);
	echo '<label class="screen-reader-text" for="proj_name">' . __('Name', 'perfect') . '</label><input name="proj_name" id="proj_name" style="width: 99%;" value="' . $proj_name . '" />';
}

add_filter('manage_edit-projects_columns', 'proj_name_edit_columns');
function proj_name_edit_columns($columns){
	$before = array_slice($columns, 0, 2);
	$new_columns = array('proj_name' => __('Name', 'perfect'));
	$after = array_slice($columns, 2);
	$columns = array_merge($before, $new_columns, $after);
	return $columns;
}
// Staff Name options
function staff_name_options(){
	global $post;
	$custom = get_post_custom($post->ID);
	$staff_name = (empty($custom['staff_name'][0]) ? '' : $custom['staff_name'][0]);
	echo '<label class="screen-reader-text" for="staff_name">' . __('Name', 'perfect') . '</label><input name="staff_name" id="staff_name" style="width: 99%;" value="' . $staff_name . '" />';
}

add_filter('manage_edit-staff_columns', 'staff_name_edit_columns');
function staff_name_edit_columns($columns){
	$before = array_slice($columns, 0, 2);
	$new_columns = array('staff_name' => __('Name', 'perfect'));
	$after = array_slice($columns, 2);
	$columns = array_merge($before, $new_columns, $after);
	return $columns;
}
// URL options
function website_url_options(){
	global $post;
	$custom = get_post_custom($post->ID);
	$website_url = (empty($custom['website_url'][0]) ? '' : $custom['website_url'][0]);
	echo '<label class="screen-reader-text" for="website_url">' . __('URL', 'perfect') . '</label><input name="website_url" id="website_url" style="width: 99%;" value="' . $website_url . '" default="#" />';
}

add_filter('manage_edit-clients_columns', 'website_url_edit_columns');
function website_url_edit_columns($columns){
	$before = array_slice($columns, 0, 2);
	$new_columns = array('website_url' => __('URL', 'perfect'));
	$after = array_slice($columns, 2);
	$columns = array_merge($before, $new_columns, $after);
	return $columns;
}
// display options
add_action('manage_posts_custom_column',  'custom_columns_display');
function custom_columns_display($columns){
	global $post;
	switch ($columns)
	{
		case 'proj_name':
			$custom = get_post_custom($post->ID);
			$proj_name = (empty($custom['proj_name'][0]) ? '' : $custom['proj_name'][0]);
			echo $proj_name;
			break;
		case 'staff_name':
			$custom = get_post_custom($post->ID);
			$staff_name = (empty($custom['staff_name'][0]) ? '' : $custom['staff_name'][0]);
			echo $staff_name;
			break;
		case 'website_url':
			$custom = get_post_custom($post->ID);
			$website_url = (empty($custom['website_url'][0]) ? '' : $custom['website_url'][0]);
			echo $website_url;
			break;
	}
}




add_action('save_post', 'custom_add_save');
function custom_add_save($postID){
	if (!defined('DOING_AUTOSAVE') && !DOING_AUTOSAVE) {
		return $postID;
	} else {
		// called after a post or page is saved and not on autosave
		if($parent_id = wp_is_post_revision($postID)){
			$postID = $parent_id;
		}

		$fields = array('slogan', 'faq_answer', 'proj_name', 'staff_name', 'website_url');

		foreach($fields as $field_name) {
			if (isset($_POST[$field_name]))
				if (!empty($_POST[$field_name])){
					update_custom_meta($postID, $_POST[$field_name], $field_name);
				} else {
					update_custom_meta($postID, '', $field_name);
				}
		}
	}

}
function update_custom_meta($postID, $new_value, $field_name) {
	// To create new meta
	if(!get_post_meta($postID, $field_name)){
		add_post_meta($postID, $field_name, $new_value);
	} else {
		// or to update existing meta
		update_post_meta($postID, $field_name, $new_value);
	}
}


// Dashboard
add_action('right_now_content_table_end', 'add_custom_counts');

function add_custom_counts() {
	if (post_type_exists( 'projects' )) {
		$num_posts = wp_count_posts( 'projects' );
		$num = number_format_i18n( $num_posts->publish );
		$text = _n( 'Projects', 'Projects', intval($num_posts->publish) );
		if ( current_user_can( 'edit_posts' ) ) {
			$num = "<a href='edit.php?post_type=projects'>$num</a>";
			$text = "<a href='edit.php?post_type=projects'>$text</a>";
		}
		echo '<td class="first b b-tags">' . $num . '</td>';
		echo '<td class="t tags">' . $text . '</td>';

		echo '</tr>';

		if ($num_posts->pending > 0) {
			$num = number_format_i18n( $num_posts->pending );
			$text = _n( 'Projects Pending', 'Projects Pending', intval($num_posts->pending) );
			if ( current_user_can( 'edit_posts' ) ) {
				$num = "<a href='edit.php?post_status=pending&post_type=projects'>$num</a>";
				$text = "<a href='edit.php?post_status=pending&post_type=projects'>$text</a>";
			}
			echo '<td class="b b-waiting">' . $num . '</td>';
			echo '<td class="last t">' . $text . '</td>';

			echo '</tr>';
		}
	}

	if (post_type_exists( 'staff' )) {
		$num_posts = wp_count_posts( 'staff' );
		$num = number_format_i18n( $num_posts->publish );
		$text = _n( 'Staff', 'Staff', intval($num_posts->publish) );
		if ( current_user_can( 'edit_posts' ) ) {
			$num = "<a href='edit.php?post_type=staff'>$num</a>";
			$text = "<a href='edit.php?post_type=staff'>$text</a>";
		}
		echo '<td class="first b b-tags">' . $num . '</td>';
		echo '<td class="t tags">' . $text . '</td>';

		echo '</tr>';

		if ($num_posts->pending > 0) {
			$num = number_format_i18n( $num_posts->pending );
			$text = _n( 'Staff Pending', 'Staff Pending', intval($num_posts->pending) );
			if ( current_user_can( 'edit_posts' ) ) {
				$num = "<a href='edit.php?post_status=pending&post_type=staff'>$num</a>";
				$text = "<a href='edit.php?post_status=pending&post_type=staff'>$text</a>";
			}
			echo '<td class="b b-waiting">' . $num . '</td>';
			echo '<td class="last t">' . $text . '</td>';

			echo '</tr>';
		}
	}

	if (post_type_exists( 'faqs' )) {
		$num_posts = wp_count_posts( 'faqs' );
		$num = number_format_i18n( $num_posts->publish );
		$text = _n( 'FAQs', 'FAQs', intval($num_posts->publish) );
		if ( current_user_can( 'edit_posts' ) ) {
			$num = "<a href='edit.php?post_type=faqs'>$num</a>";
			$text = "<a href='edit.php?post_type=faqs'>$text</a>";
		}
		echo '<td class="first b b-tags">' . $num . '</td>';
		echo '<td class="t tags">' . $text . '</td>';

		echo '</tr>';

		if ($num_posts->pending > 0) {
			$num = number_format_i18n( $num_posts->pending );
			$text = _n( 'FAQs Pending', 'FAQs Pending', intval($num_posts->pending) );
			if ( current_user_can( 'edit_posts' ) ) {
				$num = "<a href='edit.php?post_status=pending&post_type=faqs'>$num</a>";
				$text = "<a href='edit.php?post_status=pending&post_type=faqs'>$text</a>";
			}
			echo '<td class="b b-waiting">' . $num . '</td>';
			echo '<td class="last t">' . $text . '</td>';

			echo '</tr>';
		}
	}

}

// styles & scripts
if ( !is_admin() ) {
	function init_styles_and_scripts() {
		wp_register_style( 'main', get_bloginfo( 'template_directory' ) . '/style.css');

		wp_register_script( 'watermarkinput', get_bloginfo( 'template_directory' ) . '/js/jquery.watermarkinput.js', array('jquery'), '1.0' );
		wp_register_script( 'hover_intent', get_bloginfo( 'template_directory' ) . '/js/jquery.hoverIntent.js', array('jquery'), '1.0' );
		wp_register_script( 'superfish', get_bloginfo( 'template_directory' ) . '/js/superfish.js', array('jquery'), '1.4' );
		wp_register_script( 'slideshow', get_bloginfo( 'template_directory' ) . '/js/slideshow.js', array('jquery'), '1.0' );
		wp_register_script( 'jcarousel', get_bloginfo( 'template_directory' ) . '/js/jquery.jcarousel.min.js', array('jquery'), '1.0' );
		wp_register_script( 'fade', get_bloginfo( 'template_directory' ) . '/js/fade.js' );
	}

	add_action('init', 'init_styles_and_scripts');

}

?>