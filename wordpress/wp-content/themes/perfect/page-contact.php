<?php
/**
 * @package WordPress
 * @subpackage Perfect
 */
/*
 Template Name: Contact Page Layout
 */
?>
<?php get_header();?>
<?php if ( have_posts() ): the_post();
?>
<div id='the-contact'>
	<div class="content">
		<div class="title">
			<h2><?php the_title();?></h2>
		</div>
		<?php echo do_shortcode('[contact-form 1 "Contact form 1"]');?>
	</div>
	<?php endif;?>
	<?php get_sidebar('contact');?>
</div>
<?php get_footer();?>
