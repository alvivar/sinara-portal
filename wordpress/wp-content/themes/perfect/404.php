<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage Perfect
 */

get_header(); ?>

			<div class="title">
				<h2><?php _e( 'Página no disponible.', 'perfect' ); ?></h2>
			</div>


<?php get_footer(); ?>