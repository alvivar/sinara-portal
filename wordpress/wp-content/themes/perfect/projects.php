<?php
/*
Template Name: Projects Page Layout
*/
get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<div class="title">
		<h2><?php echo get_post_meta($post->ID, 'slogan', $single = true); ?></h2>
	</div>
	<?php $loop = new WP_Query(array('post_type' => 'projects', 'posts_per_page' => 9999)); ?>
	<?php if ($loop->have_posts()): ?>
		<ul id="mycarousel" class="personnel jcarousel-skin-tango">
		<?php while ($loop->have_posts()) :	$loop->the_post(); ?>
			<li>
				<a href="<?php the_permalink(); ?>">
					<span class="person_pic"><?php the_post_thumbnail('projects-thumb'); ?></span>
					<span class="person_title"><?php echo get_post_meta($post->ID, 'proj_name', $single = true); ?></span>
					<?php
						$depts = array();
						$terms = get_the_terms($post->ID, 'department');
						if (!empty($terms)) {
							foreach($terms as $term) {
								$depts[] = $term->name;
							}
							$depts = implode(',', $depts) . ' dpt';
							echo '<span class="person_dpt">' . $depts . '</span>';
						}
					?>
					<span class="person_short"><?php the_excerpt(); ?></span>
				</a>
			</li>
		<?php endwhile; ?>
		</ul>
	<?php endif; ?>
<?php endwhile; ?>
<?php get_footer(); ?>