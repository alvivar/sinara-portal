<?php
/**
 * The Template for displaying all single posts of Staff Category.
 *
 * @package WordPress
 * @subpackage Perfect
 * @since Perfect 1.0
 */

get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<div class="page_slogan">
		<h1><?php _e('Our company is comprised of a team of experts in financial and business industry', 'perfect') ?></h1>
	</div>
	<div class="person_box">
		<span class="person_pic"><?php the_post_thumbnail('staff-thumb'); ?></span>
		<span class="person_title"><?php echo get_post_meta($post->ID, 'staff_name', $single = true); ?></span>
		<?php
			$depts = array();
			$terms = get_the_terms($post->ID, 'department');
			if (!empty($terms)) {
				foreach($terms as $term) {
					$depts[] = $term->name;
				}
				$depts = implode(',', $depts) . ' dpt';
				echo '<span class="person_dpt">' . $depts . '</span>';
			}
		?>
		<span class="person_short"><?php the_excerpt(); ?></span>
	</div>
	<div class="cont_det">
		<h5><?php the_title(); ?></h5>
		<?php the_content(); ?>
	</div>
	<div class="clear"></div>	
	<?php comments_template( '', true ); ?>
	
<?php endwhile; ?>
<?php get_footer(); ?>