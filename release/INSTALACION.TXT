La aplicación es un Wordpress. Necesita una base de datos MySQL, la
configuración de la base de datos y el usuario se realiza en el archivo 
"wp-config.php" dentro de la carpeta wordpress.

Para el resto de la instalación, se debe buscar y reemplazar todos las
apariciones de: "http://localhost/sinara-portal/wordpress" en el script
SIREA.SQL por la dirección donde se encontrará la aplicación en la web.
Ejemplo: "http://sirea.com", sin colocar un "/" al final y sin las comillas.

Es recomendado mantener una relación de los permisos de los archivos en un
servidor Unix de 644 para archivos, 755 para directorios utilizando la
aplicación chmod.
