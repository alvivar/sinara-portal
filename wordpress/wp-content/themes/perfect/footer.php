<?php
/**
 * The template for displaying the footer.
 *
 * @package WordPress
 * @subpackage Perfect
 * @since Perfect 1.0
 */
?>
			<div class="clear"></div>
		</div>
		<?php get_sidebar('footer'); ?>
	</div>
</div>
<div id="footer">
	<div class="copy"><?php bloginfo( 'name' ); ?> &copy; <?php echo date('Y'); ?> <!--&bull;--> <?php //_e('Privacy Policy', 'perfect') ?></div>
	<?php wp_nav_menu(array(
			  'container' => false,
			  'menu_class' => 'f_menu',
			  'theme_location' => 'footer',
			  'depth' => 1,
	)); ?>
</div>
<?php wp_footer(); ?>
</body>
</html>
