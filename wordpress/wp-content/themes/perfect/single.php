<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Perfect
 * @since Perfect 1.0
 */

get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<div class="title">
		<h2><?php the_title(); ?></h2>
	</div>
	<?php if (has_post_thumbnail()): ?>
	<div class="read_more_pic"><?php the_post_thumbnail('single-thumb'); ?></div>
	<?php endif; ?>
	<div class="read_more_det">
		<?php the_content(); ?>
	</div>
	<div class="clear"></div>
<?php get_sidebar('single'); ?>
<div class="clear"></div>
<?php comments_template( '', true ); ?>
<?php endwhile; ?>
<?php get_footer(); ?>