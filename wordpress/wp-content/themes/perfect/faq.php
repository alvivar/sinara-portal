<?php
/*
Template Name: FAQs Page Layout
*/
get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<div class="title">
		<h2><?php the_title(); ?></h2>
	</div>
<?php $loop = new WP_Query(array('post_type' => 'faqs', 'posts_per_page' => 9999)); ?>
<?php if ($loop->have_posts()): ?>
	<?php while ($loop->have_posts()) :	$loop->the_post(); ?>
	<div class="question">
		<span class="q"><?php _e('P?', 'perfect') ?></span>
		<h5><?php the_title(); ?></h5>
	</div>
	<div class="answer">
		<span class="a"><?php _e('R.', 'perfect') ?></span>
		<?php $custom = get_post_custom($post->ID); ?>
		<?php $faq_answer = (empty($custom['faq_answer'][0]) ? '' : $custom['faq_answer'][0]); ?>
		<div class="answer_det"><?php echo $faq_answer; ?></div>
	</div>
	<?php endwhile; ?>
<?php endif; ?>
<?php endwhile; ?>
<?php get_footer(); ?>