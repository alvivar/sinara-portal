<?php
/*
 Template Name: Front Page Layout
 */
get_header();
?>
<?php get_sidebar('menu');?>
<?php get_sidebar('home');?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post();
?>
<div id='home-show'>
	<div class="content">
		<div class="box_main_page">
			<div class="title">
				<?php $custom = get_post_custom($post -> ID);?>
				<?php $slogan = (empty($custom['slogan'][0]) ? '' : $custom['slogan'][0]);?>
				<h2><?php echo $slogan;?></h2>
			</div>
			<?php $loop = new WP_Query( array('post_type' => 'foundation', 'orderby' => 'title', 'order' => 'ASC', 'numberposts' => 3));?>
			<?php $column_classes = array('column_one', 'column_two', 'column_three');?>
			<?php if ($loop->have_posts()) while ($loop->have_posts()) : $loop->the_post();
			?>
			<div class="<?php echo current($column_classes);
				next($column_classes);
			?>">
				<span class="letter"><?php the_title();?></span>
				<div class="excerpt">
					<?php the_excerpt();?>
				</div>
				<?php the_content();?>
			</div>
			<?php endwhile;?>
			<div class="clear"></div>
		</div>
		<!--
		gkHACK
		No need for selection post on home.

		<p>&nbsp;</p>
		<?php $loop = new WP_Query(array( 'post_type' => 'post', 'category_name' => 'selected', 'orderby' => 'post_date', 'order' => 'DESC', 'numberposts' => 5, 'posts_per_page' => 5 )); ?>
		<?php if ($loop->have_posts()) while ($loop->have_posts()) : $loop->the_post(); ?>
		<div class="post">
		<div class="title">
		<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
		</div>
		<p><?php the_post_thumbnail(); ?></p>
		<div class="post_short"><?php the_excerpt(); ?></div>
		</div>
		<div class="row">
		<div class="read_more"><a href="<?php the_permalink(); ?>"><?php _e('Leer más', 'perfect') ?></a></div>
		<div class="comments"><?php echo get_the_date('F j, Y'); ?></div>
		</div>
		<?php endwhile; ?>
		-->
	</div>
</div>
<?php endwhile;?>
<?php get_footer();?>