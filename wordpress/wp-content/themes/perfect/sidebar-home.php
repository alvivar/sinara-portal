<?php
/**
 * @package WordPress
 * @subpackage Perfect
 */
	if ( !is_active_sidebar( 'home-widget-area' ))
		return;
?>
<div class="sidebar">
	<?php if ( is_active_sidebar( 'home-widget-area' ) ) : ?>
		<?php dynamic_sidebar( 'home-widget-area' ); ?>
	<?php endif; ?>
</div>
